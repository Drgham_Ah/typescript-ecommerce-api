import express, { request, Request, Response } from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from "dotenv";

// Import Routers
import CategoryRouter from './src/routers/category.router';
import UserRouter from './src/routers/user.router';
import ProductRouter from './src/routers/product.router';
import AuthRouter from './src/routers/auth.router';
import CountryRouter from './src/routers/country.router';
import StateRouter from './src/routers/state.router';
import SeederRouter from './src/routers/seeder.router';
import DestroyRouter from './src/routers/destroy.router';
import AdminRouter from './src/routers/admin.router';


dotenv.config();

const app = express()

mongoose.connect(`${process.env.MONGODB_URL}`);

// Set app settings
app.use(bodyParser.json());


app.post('/test', async (req : Request, res : Response) => {

})


// Use routers
app.use('/countries', CountryRouter);
app.use('/states', StateRouter);
app.use('/auth', AuthRouter);
app.use('/categories', CategoryRouter);
app.use('/products', ProductRouter);
app.use('/users', UserRouter);
app.use('/seeder', SeederRouter);
app.use('/destroy', DestroyRouter);
app.use('/auth/admin', AdminRouter)

const port = process.env.PORT;
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});