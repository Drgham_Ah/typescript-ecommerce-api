"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.stateSeeder = exports.countrySeeder = exports.seeder = void 0;
const country_model_1 = __importDefault(require("../models/country.model"));
const countries_json_1 = __importDefault(require("../data/countries.json"));
const state_model_1 = __importDefault(require("../models/state.model"));
const states_json_1 = __importDefault(require("../data/states.json"));
const seeder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield country_model_1.default.insertMany(countries_json_1.default);
        if (result) {
            const resultTwo = yield state_model_1.default.insertMany(states_json_1.default);
            if (resultTwo) {
                return res.status(200).json({
                    success: 'Seeded Successfully',
                });
            }
        }
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.seeder = seeder;
const countrySeeder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield country_model_1.default.insertMany(countries_json_1.default);
        if (result) {
            return res.status(200).json({
                success: 'Seeded Successfully',
            });
        }
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.countrySeeder = countrySeeder;
const stateSeeder = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const newStates = states_json_1.default.map((state) => __awaiter(void 0, void 0, void 0, function* () {
            let country = yield country_model_1.default.find({ shortName: state.country });
            return ({ name: state.name, countryId: country._id });
        }));
        const result = yield state_model_1.default.insertMany(newStates);
        if (result) {
            return res.status(200).json({
                success: 'Seeded successfully',
            });
        }
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.stateSeeder = stateSeeder;
