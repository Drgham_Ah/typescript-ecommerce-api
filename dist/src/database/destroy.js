"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.destroyStates = exports.destroyCountries = void 0;
const country_model_1 = __importDefault(require("../models/country.model"));
const state_model_1 = __importDefault(require("../models/state.model"));
const destroyCountries = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const countries = yield country_model_1.default.deleteMany();
        return res.status(200).json({
            success: "Countries destoried successfully",
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.destroyCountries = destroyCountries;
const destroyStates = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const states = yield state_model_1.default.deleteMany();
        return res.status(200).json({
            success: 'States destroy successfully',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.destroyStates = destroyStates;
