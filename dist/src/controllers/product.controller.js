"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addRating = exports.ratings = exports.destroy = exports.update = exports.show = exports.create = exports.index = void 0;
const category_model_1 = __importDefault(require("../models/category.model"));
const product_model_1 = __importDefault(require("../models/product.model"));
const rating_model_1 = __importDefault(require("../models/rating.model"));
const user_model_1 = __importDefault(require("../models/user.model"));
const product_validation_1 = __importDefault(require("../validations/product.validation"));
const rating_validation_1 = __importDefault(require("../validations/rating.validation"));
/**
 * @method GET
 * @description method to get all categories
 *
 * @param req
 * @param res
 * @returns
 */
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const categories = yield product_model_1.default.find();
        return res.status(200).json(categories);
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.index = index;
/**
 * @method POST
 * @description method to create new product
 *
 * @param req
 * @param res
 * @returns
 */
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, product_validation_1.default)(req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const vendor = yield user_model_1.default.findById(value.vendorId);
        const category = yield category_model_1.default.findById(value.categoryId);
        if (!vendor || !category || vendor.role !== 'v') {
            return res.status(404).json({
                error: 'Not Found',
                message: 'Vendor id or category id may not exist',
            });
        }
        const product = yield product_model_1.default.create(value);
        if (product) {
            return res.status(201).json(product);
        }
        return res.status(501).json({ error: 'Something went wrong' });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.create = create;
/**
 * @method GET
 * @description method to get a single product
 *
 * @param req
 * @param res
 * @returns
 */
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const product = yield product_model_1.default.findById(req.params.id);
        if (product) {
            return res.status(200).json(product);
        }
        return res.status(404).json({
            error: 'Not Found',
            message: 'product may not exits',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.show = show;
/**
 * @method PUT
 * @description Method to update a single product
 *
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, product_validation_1.default)(req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const vendor = yield user_model_1.default.findById(value.vendorId);
        const category = yield category_model_1.default.findById(value.categoryId);
        if (!vendor || !category || vendor.role !== 'v') {
            return res.status(404).json({
                error: 'Not Found',
                message: 'Vendor id or category id may not exist',
            });
        }
        const product = yield product_model_1.default.findByIdAndUpdate(req.params.id, value);
        if (!product) {
            return res.status(404).json({
                error: 'Not found',
                message: 'product May not exits',
            });
        }
        return res.status(200).json({
            success: 'Updated',
            message: 'product updated successfully'
        });
    }
    catch (error) {
    }
});
exports.update = update;
/**
 * @method DELETE
 * @description method to delete one product
 *
 * @param req
 * @param res
 * @returns
 */
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const product = yield product_model_1.default.findByIdAndDelete(req.params.id);
        if (product) {
            return res.status(200).json({
                success: 'Deleted',
                message: 'product deleted successfully',
            });
        }
        return res.status(404).json({
            error: 'Not found',
            message: 'product may not exist',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.destroy = destroy;
/**
 * @method GET
 * @description method to get all product ratings
 *
 * @param req
 * @param res
 * @returns
 */
const ratings = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const ratings = yield rating_model_1.default.find({ productId: req.params.id });
        if (ratings) {
            return res.status(200).json(ratings);
        }
        return res.status(404).json({
            error: 'Not Found',
            message: 'There are no ratings for this product',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.ratings = ratings;
/**
 * @method POST
 * @description method to add new rating for the product
 *
 * @param req
 * @param res
 * @returns
 */
const addRating = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, rating_validation_1.default)(req.params.id, req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const rating = yield rating_model_1.default.create({
            productId: value.productId,
            userId: value.productId,
            title: value.title,
            description: value.description,
            rate: value.rate,
        });
        return res.status(201).json(rating);
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.addRating = addRating;
