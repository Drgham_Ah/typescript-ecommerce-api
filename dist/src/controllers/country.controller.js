"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.destroy = exports.update = exports.show = exports.create = exports.index = void 0;
const country_model_1 = __importDefault(require("../models/country.model"));
const country_validation_1 = __importDefault(require("../validations/country.validation"));
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const countries = yield country_model_1.default.find();
        return res.status(200).json(countries);
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.index = index;
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, country_validation_1.default)(req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const country = yield country_model_1.default.create(value);
        return res.status(201).json(country);
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.create = create;
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const country = yield country_model_1.default.findById(req.params.id);
        if (country) {
            return res.status(200).json(country);
        }
        return res.status(404).json({
            error: 'Not Found',
            message: 'Country may not exist',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.show = show;
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, country_validation_1.default)(req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const country = yield country_model_1.default.findByIdAndUpdate(req.params, value);
        if (country) {
            return res.status(200).json({
                success: 'Updated!',
                'message': 'Country updated successfully',
            });
        }
        return res.status(404).json({
            error: 'Not found',
            message: 'Country may not exist',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.update = update;
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const country = yield country_model_1.default.findByIdAndDelete(req.params);
        if (country) {
            return res.status(200).json({
                success: 'Deleted!',
                message: 'Country deleted successfully',
            });
        }
        return res.status(404).json({
            error: 'Not found',
            message: 'Country may not exist',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.destroy = destroy;
