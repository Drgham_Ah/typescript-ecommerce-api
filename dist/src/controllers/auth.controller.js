"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.customerRegister = void 0;
const jwt_helpers_1 = require("../helpers/jwt.helpers");
const auth_validation_1 = require("../validations/auth.validation");
const country_model_1 = __importDefault(require("../models/country.model"));
const state_model_1 = __importDefault(require("../models/state.model"));
const user_model_1 = __importDefault(require("../models/user.model"));
const bcryptjs_1 = require("bcryptjs");
const customerRegister = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, auth_validation_1.validateCustomerRegister)(req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const country = yield country_model_1.default.findById(value.countryId);
        const state = yield state_model_1.default.findById(value.stateId);
        if (!country || !state || state.countryId !== value.countryId) {
            return res.status(404).json({
                error: "Not Found",
                message: 'Country id or state id may not exist',
            });
        }
        const customer = yield user_model_1.default.create(Object.assign(Object.assign({}, value), { password: (0, bcryptjs_1.hashSync)(value.password, 10), role: 'c' }));
        if (customer) {
            const payload = {
                _id: customer._id,
                name: customer.name,
                email: customer.email,
                phone: customer.phone,
                role: 'c',
                image: "",
            };
            const token = (0, jwt_helpers_1.generateToken)(payload);
            return res.status(201).json(token);
        }
        return res.status(501).json({
            error: 'Not registerd',
            message: 'Something went wrong',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.customerRegister = customerRegister;
