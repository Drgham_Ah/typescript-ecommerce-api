"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.destroy = exports.update = exports.show = exports.create = exports.index = void 0;
const user_model_1 = __importDefault(require("../models/user.model"));
const user_validation_1 = __importDefault(require("../validations/user.validation"));
/**
 * @method GET
 * @description Method to get all users
 *
 * @param req
 * @param res
 * @returns
 */
const index = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const users = yield user_model_1.default.find();
        return res.status(200).json(users);
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.index = index;
/**
 * @method POST
 * @description method to create new user
 *
 * @param req
 * @param res
 * @returns
 */
const create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, user_validation_1.default)(req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const user = yield user_model_1.default.create(value);
        if (user) {
            return res.status(201).json(user);
        }
        return res.status(501).json({ error: "Something went wrong" });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.create = create;
/**
 * @method GET
 * @description Method to get a single user
 * @param req
 * @param res
 * @returns
 */
const show = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield user_model_1.default.findById(req.params.id);
        if (user) {
            return res.status(200).json(user);
        }
        return res.status(404).json({
            error: 'Not Found',
            message: 'User may not exist',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.show = show;
/**
 * @method PUT
 * @description method to updated a single user
 *
 * @param req
 * @param res
 * @returns
 */
const update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error, value } = (0, user_validation_1.default)(req.body);
    if (error)
        return res.status(400).json(error.details);
    try {
        const user = yield user_model_1.default.findByIdAndUpdate(req.params.id, value);
        if (user) {
            return res.status(200).json({
                success: 'Updated',
                message: 'User updated successfully',
            });
        }
        return res.status(404).json({
            error: 'Not Found',
            message: 'User may not exist',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.update = update;
/**
 * @method DELET
 * @description Method to delete a single user
 *
 * @param req
 * @param res
 * @returns
 */
const destroy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield user_model_1.default.findByIdAndDelete(req.params.id);
        if (user) {
            return res.status(200).json({
                success: 'Deleted',
                message: 'User Deleted successfully',
            });
        }
        return res.status(404).json({
            error: 'Not Found',
            message: 'User may not exist',
        });
    }
    catch (error) {
        return res.status(501).json(error);
    }
});
exports.destroy = destroy;
