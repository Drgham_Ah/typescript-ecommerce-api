"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateCustomerRegister = void 0;
const joi_1 = __importDefault(require("joi"));
const joi_objectid_1 = __importDefault(require("@marsup/joi-objectid"));
const Joi = joi_1.default.extend(joi_objectid_1.default);
const CustomerRegisterSchema = Joi.object().keys({
    name: Joi.string().alphanum().min(6).max(64).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(8).required(),
    phone: Joi.string().regex(/^\+?[1-9][0-9]{7,14}$/).min(9).max(20).required(),
    countryId: Joi.objectId().required(),
    stateId: Joi.objectId().required(),
    address: Joi.string().required(),
});
const validateCustomerRegister = (customer) => {
    return CustomerRegisterSchema.validate({
        name: customer.name,
        email: customer.email,
        password: customer.password,
        phone: customer.phone,
        countryId: customer.coutnryId,
        stateId: customer.stateId,
        address: customer.address,
    });
};
exports.validateCustomerRegister = validateCustomerRegister;
