"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const UserSchema = joi_1.default.object().keys({
    name: joi_1.default.string().alphanum().max(24).required(),
    email: joi_1.default.string().email().required(),
    password: joi_1.default.string().required(),
    phone: joi_1.default.string().required(),
    role: joi_1.default.string().valid('a', 'c', 'v').default('c'),
});
const validateUser = (user) => {
    return UserSchema.validate({
        name: user.name,
        email: user.email,
        password: user.password,
        phone: user.phone,
        role: user.role,
    });
};
exports.default = validateUser;
