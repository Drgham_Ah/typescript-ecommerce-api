"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const joi_objectid_1 = __importDefault(require("@marsup/joi-objectid"));
const Joi = joi_1.default.extend(joi_objectid_1.default);
const StateSchema = Joi.object().keys({
    name: Joi.string().required(),
    countryId: Joi.objectId().required(),
});
const validateState = (state) => {
    return StateSchema.validate({
        name: state.name,
        countryId: state.countryId,
    });
};
exports.default = validateState;
