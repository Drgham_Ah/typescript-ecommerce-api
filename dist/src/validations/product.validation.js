"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const joi_objectid_1 = __importDefault(require("@marsup/joi-objectid"));
const Joi = joi_1.default.extend(joi_objectid_1.default);
const CreatingProductSchema = Joi.object().keys({
    name: Joi.string().required(),
    categoryId: Joi.objectId().required(),
    vendorId: Joi.objectId().required(),
    price: Joi.number().required(),
    quantity: Joi.number().required(),
});
const validateProduct = (product) => {
    return CreatingProductSchema.validate({
        name: product.name,
        categoryId: product.categoryId,
        vendorId: product.vendorId,
        price: product.price,
        quantity: product.quantity,
    }, { abortEarly: false });
};
exports.default = validateProduct;
