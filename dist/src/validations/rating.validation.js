"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const joi_objectid_1 = __importDefault(require("@marsup/joi-objectid"));
const Joi = joi_1.default.extend(joi_objectid_1.default);
const RatingSchema = Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string(),
    rate: Joi.number().required,
    userId: Joi.objectId().required(),
    productId: Joi.objectId().required(),
});
const validateRating = (productId, rating) => {
    return RatingSchema.validate({
        productId: productId,
        userId: rating.userId,
        title: rating.title,
        description: rating.description,
    });
};
exports.default = validateRating;
