"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateToken = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const jsonwebtoken_1 = require("jsonwebtoken");
dotenv_1.default.config();
const generateToken = (payloads) => {
    const token = (0, jsonwebtoken_1.sign)(payloads, `${process.env.JWT_SECRETKEYS}`);
    return token;
};
exports.generateToken = generateToken;
