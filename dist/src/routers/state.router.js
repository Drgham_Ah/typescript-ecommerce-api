"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const state_controller_1 = require("../controllers/state.controller");
const StateRouter = express_1.default.Router();
StateRouter.get('/', state_controller_1.index);
StateRouter.post('/', state_controller_1.create);
StateRouter.get('/:id', state_controller_1.show);
StateRouter.put('/:id', state_controller_1.update);
StateRouter.delete('/:id', state_controller_1.destroy);
exports.default = StateRouter;
