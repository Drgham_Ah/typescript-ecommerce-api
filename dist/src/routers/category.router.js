"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const category_controller_1 = require("../controllers/category.controller");
const CategoryRouter = express_1.default.Router();
CategoryRouter.get('/', category_controller_1.index);
CategoryRouter.post('/', category_controller_1.create);
CategoryRouter.get('/:id', category_controller_1.show);
CategoryRouter.put('/:id', category_controller_1.update);
CategoryRouter.delete('/:id', category_controller_1.destroy);
exports.default = CategoryRouter;
