"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const seeder_1 = require("../database/seeder");
const SeederRouter = express_1.default.Router();
SeederRouter.post('/', seeder_1.seeder);
SeederRouter.post('/countries', seeder_1.countrySeeder);
SeederRouter.post('/states', seeder_1.stateSeeder);
exports.default = SeederRouter;
