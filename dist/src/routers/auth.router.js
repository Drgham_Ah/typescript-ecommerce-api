"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const auth_controller_1 = require("../controllers/auth.controller");
const AuthRouter = express_1.default.Router();
AuthRouter.post('/customer/register', auth_controller_1.customerRegister);
exports.default = AuthRouter;
