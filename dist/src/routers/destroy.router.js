"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const destroy_1 = require("../database/destroy");
const DestroyRouter = express_1.default.Router();
DestroyRouter.post('/countries', destroy_1.destroyCountries);
DestroyRouter.post('/states', destroy_1.destroyStates);
exports.default = DestroyRouter;
