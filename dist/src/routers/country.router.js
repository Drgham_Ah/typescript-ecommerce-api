"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const country_controller_1 = require("../controllers/country.controller");
const CountryRouter = express_1.default.Router();
CountryRouter.get('/', country_controller_1.index);
CountryRouter.post('/', country_controller_1.create);
CountryRouter.get('/:id', country_controller_1.show);
CountryRouter.put('/:id', country_controller_1.update);
CountryRouter.delete('/:id', country_controller_1.destroy);
exports.default = CountryRouter;
