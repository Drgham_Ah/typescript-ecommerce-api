"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const product_controller_1 = require("../controllers/product.controller");
const ProductRouter = express_1.default.Router();
ProductRouter.get('/', product_controller_1.index);
ProductRouter.post('/', product_controller_1.create);
ProductRouter.get('/:id', product_controller_1.show);
ProductRouter.put('/:id', product_controller_1.update);
ProductRouter.delete('/:id', product_controller_1.destroy);
exports.default = ProductRouter;
