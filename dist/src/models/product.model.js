"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const ProductSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    categoryId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: "Category",
    },
    vendorId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: "User",
    },
    quantity: {
        type: Number,
    },
    price: {
        type: Number,
    },
    image: {
        type: String,
    },
}, { timestamps: true });
const Product = mongoose_1.default.model('Product', ProductSchema);
exports.default = Product;
