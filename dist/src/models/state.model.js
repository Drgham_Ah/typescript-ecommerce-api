"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const StateSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    countryId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'Country',
    }
}, { timestamps: true });
const State = mongoose_1.default.model('State', StateSchema);
exports.default = State;
