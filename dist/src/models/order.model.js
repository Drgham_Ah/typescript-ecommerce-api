"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
// Order mongoose Schema 
const OrderSchema = new mongoose_1.default.Schema({
    customerId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    vendorId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    shipperId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'Shipper',
        required: true,
    },
    branchId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: "Branch",
    },
    address: {
        type: String,
    },
    products: [
        {
            id: {
                type: mongoose_1.default.Schema.Types.ObjectId,
                ref: 'Product',
            },
            qunatity: {
                type: Number
            },
        }
    ],
}, { timestamps: true });
const Order = mongoose_1.default.model('Order', OrderSchema);
exports.default = Order;
