"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
// Branch mongoose Schema
const BranchSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
    },
    shipperId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'Shipper',
        unique: true,
        required: true,
    },
    countryId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: "Country",
        required: true,
    },
    stateId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'State',
        required: true,
    },
}, { timestamps: true });
const Branch = mongoose_1.default.model('Branch', BranchSchema);
exports.default = Branch;
