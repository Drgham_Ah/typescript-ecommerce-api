"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
// User mongoose Schema
const UserSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String, // a for admin , v for vendor, c for customer
    },
    phone: {
        type: String,
        required: true,
    },
    image: {
        type: String,
    },
    address: {
        type: String,
    },
    countryId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'Country',
    },
    stateId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: 'State',
    }
}, { timestamps: true });
const User = mongoose_1.default.model('User', UserSchema);
exports.default = User;
