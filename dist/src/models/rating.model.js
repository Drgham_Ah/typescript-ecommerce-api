"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const RatingSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    rate: {
        type: Number,
        default: 0,
    },
    userId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    },
    productId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        required: true,
    }
}, { timestamps: true });
const Rating = mongoose_1.default.model('Rating', RatingSchema);
exports.default = Rating;
