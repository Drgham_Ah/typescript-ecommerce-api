"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
// Shipper mongoose Schema
const ShipperSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    phoneOne: {
        type: String,
        required: true,
    },
    phoneTwo: {
        type: String,
    },
}, { timestamps: true });
const Shipper = mongoose_1.default.model('Shipper', ShipperSchema);
exports.default = Shipper;
