"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const body_parser_1 = __importDefault(require("body-parser"));
const dotenv_1 = __importDefault(require("dotenv"));
// Import Routers
const category_router_1 = __importDefault(require("./src/routers/category.router"));
const user_router_1 = __importDefault(require("./src/routers/user.router"));
const product_router_1 = __importDefault(require("./src/routers/product.router"));
const auth_router_1 = __importDefault(require("./src/routers/auth.router"));
const country_router_1 = __importDefault(require("./src/routers/country.router"));
const state_router_1 = __importDefault(require("./src/routers/state.router"));
const seeder_router_1 = __importDefault(require("./src/routers/seeder.router"));
const destroy_router_1 = __importDefault(require("./src/routers/destroy.router"));
dotenv_1.default.config();
const app = (0, express_1.default)();
mongoose_1.default.connect(`${process.env.MONGODB_URL}`);
// Set app settings
app.use(body_parser_1.default.json());
app.post('/test', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
}));
// Use routers
app.use('/countries', country_router_1.default);
app.use('/states', state_router_1.default);
app.use('/auth', auth_router_1.default);
app.use('/categories', category_router_1.default);
app.use('/products', product_router_1.default);
app.use('/users', user_router_1.default);
app.use('/seeder', seeder_router_1.default);
app.use('/destroy', destroy_router_1.default);
const port = process.env.PORT;
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
