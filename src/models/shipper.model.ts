import mongoose from "mongoose";

// Shipper mongoose Schema
const ShipperSchema = new mongoose.Schema({
	name : {
		type : String,
		required : true,
		unique : true,
	},
	email : {
		type : String,
		required : true,
		unique : true,
	},
	phoneOne : {
		type : String,
		required : true,
	},
	phoneTwo : {
		type : String,
	},
}, {timestamps : true});

const Shipper = mongoose.model('Shipper', ShipperSchema);

export default Shipper;