import mongoose from 'mongoose';

// User mongoose Schema
const UserSchema = new mongoose.Schema({
	name : {
		type : String,
		required : true,
		unique : true,
	},
	email : {
		type : String,
		required : true,
		unique : true,
	},
	password : {
		type : String,
		required : true,
	},
	role : {
		type : String, // a for admin , v for vendor, c for customer
		default : 'c',
		enum : ['c', 'v', 'a'],
	},
	phone : {
		type : String,
		required : true,
	},
	image : {
		type : String,
	},
	address : {
		type : String,
	},
	countryId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : 'Country',
	},
	stateId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : 'State',
	}
}, {timestamps : true});

const User = mongoose.model('User', UserSchema);

export default User;