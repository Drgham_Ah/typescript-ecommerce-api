import mongoose from "mongoose";

const ProductSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        unique : true,
    },
    categoryId : {
        type : mongoose.Schema.Types.ObjectId,
        ref : "Category",
    },
    vendorId : {
        type : mongoose.Schema.Types.ObjectId,
        ref : "User",
    },
    quantity : {
        type : Number,
    },
    price : {
        type : Number,
    },
    image : {
        type : String,
    },
}, { timestamps : true })

const Product = mongoose.model('Product', ProductSchema);

export default Product;