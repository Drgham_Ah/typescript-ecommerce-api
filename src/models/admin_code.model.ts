import mongoose from "mongoose";

const AdminCodeSchema = new mongoose.Schema({
    code : {
        type : String,
        required : true,
        unique : true,
        length : 12
    },
    status : {
        type : String,
        default : 'u',
        enum : ['u', 'v'], // U for unverfied verfied
    }
})

const AdminCode = mongoose.model('AdminCode', AdminCodeSchema);

export default AdminCode;