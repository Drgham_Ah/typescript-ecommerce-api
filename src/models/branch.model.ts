import mongoose from "mongoose";

// Branch mongoose Schema
const BranchSchema = new mongoose.Schema({
	name : {
		type : String,
		required : true,
	},
	shipperId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : 'Shipper',
		unique : true,
		required : true,
	},
	countryId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : "Country",
		required : true,
	},
	stateId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : 'State',
		required : true,
	},
}, {timestamps : true});

const Branch = mongoose.model('Branch', BranchSchema);

export default Branch;