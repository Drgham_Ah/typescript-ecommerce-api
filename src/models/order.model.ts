import mongoose from "mongoose";

// Order mongoose Schema 
const OrderSchema = new mongoose.Schema({
	customerId : {
		type : mongoose.Schema.Types.ObjectId,
		required : true,
	},
	vendorId : {
		type : mongoose.Schema.Types.ObjectId,
		required : true,
	},
	shipperId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : 'Shipper',
		required : true,
	},
	branchId : {
		type : mongoose.Schema.Types.ObjectId,
		ref : "Branch",
	},
	address : {
		type : String,
	},
	products : [
		{
			id : {
				type : mongoose.Schema.Types.ObjectId,
				ref : 'Product',
			},
			qunatity : {
				type : Number
			},
		}
	],
}, { timestamps : true });


const Order = mongoose.model('Order', OrderSchema);

export default Order;
