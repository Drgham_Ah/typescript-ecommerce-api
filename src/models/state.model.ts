import mongoose from "mongoose";

const StateSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        unique : true,
    },
    countryId : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Country',
    }
}, {timestamps : true})

const State = mongoose.model('State', StateSchema);

export default State;