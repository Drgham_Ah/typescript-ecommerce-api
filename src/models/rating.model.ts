import mongoose from 'mongoose';

const RatingSchema = new mongoose.Schema({
    title : {
        type : String,
        required : true,
    },
    description : {
        type : String,
    },
    rate : {
        type : Number,
        default : 0,
    },
    userId : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
    },
    productId : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
    }
}, {timestamps : true})

const Rating = mongoose.model('Rating', RatingSchema);

export default Rating;