import { hashSync } from "bcryptjs";
import { generateToken } from "../helpers/jwt.helpers";
import State from "../models/state.model";
import User from "../models/user.model";
import { validateUserRegister } from "../validations/auth.validation";

interface UserInterface {
    name : string,
    email : string,
    password : string,
    phone : string,
    countryId : string,
    stateId : string,
    address : string,
    role : string,
}

export const registerUser = async (user : UserInterface, getCountryById : any) => {
    const {error, value} = validateUserRegister(user);

    if (error) return { state : 400, payload : error.details};

    try {
        const country = getCountryById(value.countryId);
        const state = await State.findById(value.stateId);

        if (!country || !state) {
            return {
                state : 404,
                payload : {
                    error : 'Not found',
                    message : "country or state may not exist",
                }
            }
        }
        const user = await User.create({...value, password : hashSync(value.password, 10), role : 'c'});
        if (user) {
            const payload = {
                _id : user._id,
                name : user.name,
                email : user.email,
                phone : user.phone,
                role : user.role,
                impage : ""
            }
            const token = generateToken(payload);
            return {
                state : 200,
                payload : {
                    token : token,
                }
            }
        }
        return {
            state : 501,
            paylod : {
                error : 'Not registerd',
                message : 'Something went wrong',
            }
        }
    } catch (error) {
        return {
            state : 501,
            payload : error
        };
    }
}