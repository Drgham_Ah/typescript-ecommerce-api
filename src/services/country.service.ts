import Country from "../models/country.model"

export const getCountryById = async (id : string) => {
    try {
        const country = await Country.findById(id);
        if (country) {
            return country
        }
    } catch (error) {
        throw new Error('Country may not exist');
    }
}