import BaseJoi from 'joi';
import JoiObjectId from '@marsup/joi-objectid';

const Joi = BaseJoi.extend(JoiObjectId);

const RatingSchema = Joi.object().keys({
    title : Joi.string().required(),
    description : Joi.string(),
    rate : Joi.number().required,
    userId : Joi.objectId().required(),
    productId : Joi.objectId().required(),
});

const validateRating = (productId : string, rating : any) => {
    return RatingSchema.validate({
        productId : productId,
        userId : rating.userId,
        title : rating.title,
        description : rating.description,
    })
}

export default validateRating;