import BaseJoi from 'joi';
import JoiObjectId from '@marsup/joi-objectid';

const Joi = BaseJoi.extend(JoiObjectId);

const UserRegisterSchema = Joi.object().keys({
    name : Joi.string().alphanum().min(6).max(64).required(),
    email : Joi.string().email().required(),
    password : Joi.string().min(8).required(),
    phone : Joi.string().regex(/^\+?[1-9][0-9]{7,14}$/).min(9).max(20).required(),
    countryId : Joi.objectId().required(),
    stateId : Joi.objectId().required(),
    address : Joi.string().required(),
    role : Joi.string().valid('c', 'v'),
});

const UserLoginSchema = Joi.object().keys({
    email : Joi.string().email().required(),
    password : Joi.string().required(),
})

export const validateUserRegister = (customer : any) => {
    return UserRegisterSchema.validate({
        name : customer.name,
        email : customer.email,
        password : customer.password,
        phone : customer.phone,
        countryId : customer.countryId,
        stateId : customer.stateId,
        address : customer.address,
    })
}

export const validateUserLogin = (user : any) => {
    return UserLoginSchema.validate({
        email : user.email,
        password : user.password,
    });
}
