import BaseJoi from 'joi'
import JoiObjectId from '@marsup/joi-objectid';

const Joi = BaseJoi.extend(JoiObjectId);

const StateSchema = Joi.object().keys({
    name : Joi.string().required(),
    countryId : Joi.objectId().required(), 
});

const validateState = (state : any) => {
    return StateSchema.validate({
        name : state.name,
        countryId : state.countryId,
    });
}

export default validateState;