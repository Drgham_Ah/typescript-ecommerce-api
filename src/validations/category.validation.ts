import Joi from 'joi';

const CategorySchema = Joi.object().keys({
    name : Joi.string().required(),
})

export const validateCategory = (cat : any) => {
    return CategorySchema.validate({
        name : cat.name,
    }, {abortEarly : false})
}
