import Joi from 'joi';

const AdminCodeSchema = Joi.object().keys({
    code : Joi.string().length(12).required(),
});

export const adminCodeValidator = (code : string) => {
    return AdminCodeSchema.validate({
        code : code,
    })
}
