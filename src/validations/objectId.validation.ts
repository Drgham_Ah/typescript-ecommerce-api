import BaseJoi from 'joi';
import JoiObjectId from '@marsup/joi-objectid';

const Joi = BaseJoi.extend(JoiObjectId);

const ObjectIdSchema = Joi.object().types({
    id : Joi.objectId().required(),
})

const validateObjectId = (id : any) => {
    return ObjectIdSchema.validate({
        id : id,
    })
}

export default validateObjectId;