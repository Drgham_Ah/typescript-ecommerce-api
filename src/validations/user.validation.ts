import Joi from 'joi';

const UserSchema = Joi.object().keys({
    name : Joi.string().alphanum().max(24).required(),
    email : Joi.string().email().required(),
    password : Joi.string().required(),
    phone : Joi.string().required(),
    role : Joi.string().valid('a', 'c', 'v').default('c'),
    
})

const validateUser = (user : any) => {
    return UserSchema.validate({
        name : user.name,
        email : user.email,
        password : user.password,
        phone : user.phone,
        role : user.role,
    })
}

export default validateUser;