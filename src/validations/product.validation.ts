import BaseJoi from 'joi';
import JoiObjectId from '@marsup/joi-objectid';
const Joi = BaseJoi.extend(JoiObjectId);

const CreatingProductSchema = Joi.object().keys({
    name : Joi.string().required(),
    categoryId : Joi.objectId().required(),
    vendorId : Joi.objectId().required(),
    price : Joi.number().required(),
    quantity : Joi.number().required(),
});

const validateProduct = (product : any) => {
    return CreatingProductSchema.validate({
        name : product.name,
        categoryId : product.categoryId,
        vendorId : product.vendorId,
        price : product.price,
        quantity : product.quantity,
    }, {abortEarly : false})
}

export default validateProduct;