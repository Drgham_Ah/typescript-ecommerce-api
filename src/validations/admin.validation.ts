import Joi from "joi";

const AdminRegisterSchema = Joi.object().keys({
    name : Joi.string().required(),
    email : Joi.string().email().required(),
    password : Joi.string().required(),
    confirmPassword : Joi.string().required(),
    phone : Joi.string().required(),
    code : Joi.string().length(12).required()
});

const AdminLoginSchema = Joi.object().keys({
    email : Joi.string().email().required(),
    password : Joi.string().required(),
});

const ChangePasswordSchema = Joi.object().keys({
    email : Joi.string().email().required(),
    oldPassword : Joi.string().required(),
    password : Joi.string().required(),
    confirmPassword : Joi.string().required(),
});

export const AdminRegisterValidator = (admin : any) => {
    return AdminRegisterSchema.validate({
        name : admin.name,
        email : admin.email,
        password : admin.password,
        confirmPassword : admin.confirmPassword,
        phone : admin.phone,
        code : admin.code,
    }, {abortEarly : false})
}

export const AdminLoginValidator = (admin : any) => {
    return AdminLoginSchema.validate({
        email : admin.email,
        password : admin.password
    })
}

export const ChangePasswordValidator = (data : any) => {
    return ChangePasswordSchema.validate({
        email : data.email,
        oldPassword : data.oldPassword,
        password : data.password,
        confirmPassword : data.confirmPassword,
    })
}
