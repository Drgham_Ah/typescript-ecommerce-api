import Joi from 'joi';

const CountrySchema = Joi.object().keys({
    name : Joi.string().required(),
    shortName : Joi.string().required(),
})

const validateCountry = (country : any) => {
    return CountrySchema.validate({
        name : country.name,
        shortName : country.shortName,
    })
}

export default validateCountry;