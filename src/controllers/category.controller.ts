import {  Request, Response } from 'express';
import Category from '../models/category.model';
import { validateCategory } from '../validations/category.validation';

/**
 * @method GET
 * @description method to get all categories
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const index = async (req : Request, res : Response) => {
    try {
        const categories = await Category.find();
        return res.status(200).json(categories);
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method POST
 * @description method to create new category
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const create = async (req : Request, res : Response) => {
    const {error, value} = validateCategory(req.body);

    if (error) return res.status(400).json(error.details);

    try {
        const category = await Category.create(value)
        if (category) {
            return res.status(201).json(category);
        }
        return res.status(501).json({error : 'Something went wrong'})
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method GET
 * @description method to get a single category
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const show = async (req : Request, res : Response) => {
    try {
        const category = await Category.findById(req.params.id);
        if (category) {
            return res.status(200).json(category);
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'Category may not exits',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method PUT
 * @description Method to update a single category
 * 
 * @param {Request} req 
 * @param {Response} res 
 * @returns 
 */
export const update = async (req : Request, res : Response) => {
    const {error, value} = validateCategory(req.body);
    if (error) return res.status(400).json(error.details);
    try {
        const category = await Category.findByIdAndUpdate(req.params.id, value);
        if (!category) {
            return res.status(404).json({
                error : 'Not found',
                message : 'Category May not exits',
            })
        }
        return res.status(200).json({
            success : 'Updated',
            message : 'Category updated successfully'
        });
    } catch (error) {

    }
}

/**
 * @method DELETE
 * @description method to delete one category
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const destroy = async (req : Request, res : Response) => {
    try {
        const category = await Category.findByIdAndDelete(req.params.id);
        if (category) {
            return res.status(200).json({
                success : 'Deleted',
                message : 'Category deleted successfully',
            });
        }
        return res.status(404).json({
            error : 'Not found',
            message : 'Category may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}