import { Request, Response } from "express";
import { generateToken } from "../helpers/jwt.helpers";
import { validateUserRegister, validateUserLogin } from "../validations/auth.validation";
import Country from '../models/country.model';
import State from "../models/state.model";
import User from "../models/user.model";
import { hashSync, compareSync } from "bcryptjs";


/**
 * @method POST
 * @description method to register new user as a vendor or customer
 * @param req 
 * @param res 
 * @returns 
 */
export const userRegister = async (req : Request, res : Response) => {
    const {error, value} = validateUserRegister(req.body);

    if (error) return res.status(400).json(error.details);

    try {
        const country = await Country.findById(value.countryId);
        const state = await State.findById(value.stateId);

        if (!country || !state) {
            return {
                state : 404,
                payload : {
                    error : 'Not found',
                    message : "country or state may not exist",
                }
            }
        }
        const user = await User.create({...value, password : hashSync(value.password, 10)});
        if (user) {
            const payloads = {
                _id : user._id,
                name : user.name,
                email : user.email,
                phone : user.phone,
                role : user.role,
                impage : ""
            }
            const token = generateToken(payloads);
            return {
                state : 200,
                payload : {
                    token : token,
                }
            }
        }
        return {
            state : 501,
            paylod : {
                error : 'Not registerd',
                message : 'Something went wrong',
            }
        }
    } catch (error) {
        return {
            state : 501,
            payload : error
        };
    }
}

/**
 * @method POST
 * @description Method to login a user as a vendor or customer
 * @param req 
 * @param res 
 * @returns 
 */
export const userLogin = async (req : Request, res : Response) => {
    const {error, value} = validateUserLogin(req.body);
    if (error) return res.status(400).json(error.details);

    try {
        const user = await User.findOne({email : value.email});
        if (!user || compareSync(value.password, user.password)) {
            return res.status(400).json({
                error : "User email or password does not match",
            })
        }
        const payloads = {
            _id : user._id,
            name : user.name,
            email : user.email,
            phone : user.phone,
            role : user.role,
            image : "",
        }
        return res.status(200).json({
            token : generateToken(payloads),
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}
