import { Request, Response } from "express";
import User from "../models/user.model";
import validateUser from "../validations/user.validation";

/**
 * @method GET
 * @description Method to get all users
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const index = async (req : Request, res : Response) => {
    try {
        const users = await User.find()
        return res.status(200).json(users);
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method POST
 * @description method to create new user
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const create = async (req: Request, res : Response) => {
    const {error, value} = validateUser(req.body);
    if (error) return res.status(400).json(error.details);

    try {
        const user = await User.create(value)
        if (user) {
            return res.status(201).json(user);
        }
        return res.status(501).json({error : "Something went wrong"});
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method GET
 * @description Method to get a single user
 * @param req 
 * @param res 
 * @returns 
 */
export const show = async (req : Request, res : Response) => {
    try {
        const user = await User.findById(req.params.id);
        if (user) {
            return res.status(200).json(user);
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'User may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method PUT
 * @description method to updated a single user
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const update = async (req : Request, res : Response) => {
    const {error, value} = validateUser(req.body);
    if (error) return res.status(400).json(error.details);
    try {
        const user = await User.findByIdAndUpdate(req.params.id, value);
        if (user) {
            return res.status(200).json({
                success : 'Updated',
                message : 'User updated successfully',
            })
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'User may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method DELET
 * @description Method to delete a single user
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const destroy = async (req : Request, res : Response) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        if (user) {
            return res.status(200).json({
                success : 'Deleted',
                message : 'User Deleted successfully',
            })
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'User may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}