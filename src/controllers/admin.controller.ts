import { Request, response, Response } from "express";
import { adminCodeValidator } from "../validations/admin-code.validation";
import randomstring from 'randomstring';
import AdminCode from "../models/admin_code.model";
import { AdminLoginValidator, AdminRegisterValidator, ChangePasswordValidator } from "../validations/admin.validation";
import Admin from "../models/admin.model";
import { compareSync, hashSync } from "bcryptjs";
import { generateToken } from "../helpers/jwt.helpers";


export const index = async (req : Request, res : Response) => {
    try {
        const admins = await Admin.find();
        if (!admins) {
            return res.status(404).json({
                error : 'Not Found',
                message : "There are no admins",
            })
        }
        return res.status(200).json(admins);
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const show = async (req : Request, res : Response) => {
    try {
        const admin = await Admin.findById(req.params.id);
        if (admin) {
            return res.status(200).json({
                _id : admin._id,
                name : admin.name,
                email : admin.email,
                phone : admin.phone
            });
        }
        return res.status(404).json({
            error : "Not Found",
            message : 'Admin may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const createAdminCode = async (req : Request, res : Response) => {
    const {error, value} = adminCodeValidator(randomstring.generate(12));
    if (error) return res.status(400).json(error.details);

    try {
        const code = await AdminCode.create(value);
        if (code) {
            return res.status(201).json({
                code : code,
            });
        }
        return res.status(501).json({
            error : "Code doesn't generated",
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const register = async (req : Request, res : Response) => {
    const {error, value} = AdminRegisterValidator(req.body);
    if (error) return res.status(400).json(error);

    if (value.password !== value.confirmPassword) return res.status(400).json({
        error : "Not valid data",
        message : "Passwords don't match",
    })

    try {
        const code = await AdminCode.findOne({code : value.code});
        if (!code) {
            return res.status(400).json({
                error : "Not allowed",
                message : "You should have admin code to register",
            })
        }
        const admin = await Admin.create({...value, password : hashSync(value.password)});
        
        const payloads = {
            _id : admin._id,
            name : admin.name,
            email : admin.email,
            phone : admin.phone,
            role : 'a',
        };

        return res.status(201).json({
            token : generateToken(payloads),
        });
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const login = async (req : Request, res : Response) => {
    const {error, value} = AdminLoginValidator(req.body);
    if (error) return res.status(400).json(error.details);

    try {
        const admin = await Admin.findOne({email : value.email});
        if (!admin || !compareSync(value.password, admin.password)) {
            return res.status(400).json({
                error : "Not valid data",
                message : "Email may not exist or password doesn't match",
            });
        }

        const payloads = {
            _id : admin._id,
            name : admin.name,
            email : admin.email,
            phone : admin.phone,
            role : "a",
        }
        return res.status(200).json({
            token : generateToken(payloads),
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const changePassword = async (req : Request, res : Response) => {
    const {error, value} = ChangePasswordValidator(req.body);
    if (error) return res.status(400).json(error.details);

    if (value.password !== value.confirmPassword) {
        return res.status(400).json({
            error : 'Not valid data',
            messgae : "Passwords are not same",
        })
    }

    try {
        const admin = await Admin.findOne({email : value.email});
        if (!admin || compareSync(value.password, admin.password)) {
            return res.status(400).json({
                error : "Not Valid Data",
                message : "Email may not exist or old password doesn't match",
            })
        }
        admin.password = hashSync(value.password);
        await admin.save();
        return res.status(200).json({
            success : 'Changed',
            password : 'Password changed successfully',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}