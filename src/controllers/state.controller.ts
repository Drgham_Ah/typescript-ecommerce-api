import {  Request, Response } from 'express';
import State from '../models/state.model';
import validateState from '../validations/state.validation';

/**
 * @method GET
 * @description method to get all categories
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const index = async (req : Request, res : Response) => {
    try {
        const categories = await State.find();
        return res.status(200).json(categories);
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method POST
 * @description method to create new state
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const create = async (req : Request, res : Response) => {
    const {error, value} = validateState(req.body);

    if (error) return res.status(400).json(error.details);

    try {
        const state = await State.create(value)
        if (state) {
            return res.status(201).json(state);
        }
        return res.status(501).json({error : 'Something went wrong'})
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method GET
 * @description method to get a single state
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const show = async (req : Request, res : Response) => {
    try {
        const state = await State.findById(req.params.id);
        if (state) {
            return res.status(200).json(state);
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'state may not exits',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method PUT
 * @description Method to update a single state
 * 
 * @param {Request} req 
 * @param {Response} res 
 * @returns 
 */
export const update = async (req : Request, res : Response) => {
    const {error, value} = validateState(req.body);
    if (error) return res.status(400).json(error.details);
    try {
        const state = await State.findByIdAndUpdate(req.params.id, value);
        if (!state) {
            return res.status(404).json({
                error : 'Not found',
                message : 'state May not exits',
            })
        }
        return res.status(200).json({
            success : 'Updated',
            message : 'state updated successfully'
        });
    } catch (error) {

    }
}

/**
 * @method DELETE
 * @description method to delete one state
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const destroy = async (req : Request, res : Response) => {
    try {
        const state = await State.findByIdAndDelete(req.params.id);
        if (state) {
            return res.status(200).json({
                success : 'Deleted',
                message : 'state deleted successfully',
            });
        }
        return res.status(404).json({
            error : 'Not found',
            message : 'state may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}