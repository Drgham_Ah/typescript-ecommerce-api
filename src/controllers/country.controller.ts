import { Request, Response } from "express";
import Country from "../models/country.model";
import validateCountry from "../validations/country.validation";

export const index = async (req : Request, res : Response) => {
    try {
        const countries = await Country.find();
        return res.status(200).json(countries);
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const create = async (req : Request, res : Response) => {
    const {error, value} = validateCountry(req.body);
    if (error) return res.status(400).json(error.details);

    try {
        const country = await Country.create(value);
        return res.status(201).json(country);
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const show = async (req : Request, res : Response) => {
    try {
        const country = await Country.findById(req.params.id);
        if (country) {
            return res.status(200).json(country);
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'Country may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const update = async (req : Request, res : Response) => {
    const {error, value} = validateCountry(req.body);
    if (error) return res.status(400).json(error.details);

    try {
        const country = await Country.findByIdAndUpdate(req.params, value);
        if (country) {
            return res.status(200).json({
                success : 'Updated!',
                'message' : 'Country updated successfully',
            })
        }
        return res.status(404).json({
            error : 'Not found',
            message : 'Country may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);       
    }
}

export const destroy = async (req : Request, res : Response) => {
    try {
        const country = await Country.findByIdAndDelete(req.params);
        if (country) {
            return res.status(200).json({
                success : 'Deleted!',
                message : 'Country deleted successfully',
            })
        }
        return res.status(404).json({
            error : 'Not found',
            message : 'Country may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}
