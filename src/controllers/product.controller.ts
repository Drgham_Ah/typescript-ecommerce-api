import {  Request, Response } from 'express';
import Category from '../models/category.model';
import Product from '../models/product.model';
import Rating from '../models/rating.model';
import User from '../models/user.model';
import validateProduct from '../validations/product.validation';
import validateRating from '../validations/rating.validation';

/**
 * @method GET
 * @description method to get all categories
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const index = async (req : Request, res : Response) => {
    try {
        const categories = await Product.find();
        return res.status(200).json(categories);
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method POST
 * @description method to create new product
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const create = async (req : Request, res : Response) => {
    const {error, value} = validateProduct(req.body);

    if (error) return res.status(400).json(error.details);

    try {
        const vendor = await User.findById(value.vendorId);
        const category = await Category.findById(value.categoryId);
    
        if (!vendor || !category || vendor.role !== 'v') {
            return res.status(404).json({
                error : 'Not Found',
                message : 'Vendor id or category id may not exist',
            });
        }

        const product = await Product.create(value)
        if (product) {
            return res.status(201).json(product);
        }
        return res.status(501).json({error : 'Something went wrong'})
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method GET
 * @description method to get a single product
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const show = async (req : Request, res : Response) => {
    try {
        const product = await Product.findById(req.params.id);
        if (product) {
            return res.status(200).json(product);
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'product may not exits',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method PUT
 * @description Method to update a single product
 * 
 * @param {Request} req 
 * @param {Response} res 
 * @returns 
 */
export const update = async (req : Request, res : Response) => {
    const {error, value} = validateProduct(req.body);
    if (error) return res.status(400).json(error.details);

    try {
        const vendor = await User.findById(value.vendorId);
        const category = await Category.findById(value.categoryId);
    
        if (!vendor || !category || vendor.role !== 'v') {
            return res.status(404).json({
                error : 'Not Found',
                message : 'Vendor id or category id may not exist',
            });
        }

        const product = await Product.findByIdAndUpdate(req.params.id, value);
        if (!product) {
            return res.status(404).json({
                error : 'Not found',
                message : 'product May not exits',
            })
        }
        return res.status(200).json({
            success : 'Updated',
            message : 'product updated successfully'
        });
    } catch (error) {

    }
}

/**
 * @method DELETE
 * @description method to delete one product
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const destroy = async (req : Request, res : Response) => {
    try {
        const product = await Product.findByIdAndDelete(req.params.id);
        if (product) {
            return res.status(200).json({
                success : 'Deleted',
                message : 'product deleted successfully',
            });
        }
        return res.status(404).json({
            error : 'Not found',
            message : 'product may not exist',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method GET
 * @description method to get all product ratings
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const ratings = async (req : Request, res : Response) => {
    try {
        const ratings = await Rating.find({productId : req.params.id});
        if (ratings) {
            return res.status(200).json(ratings);
        }
        return res.status(404).json({
            error : 'Not Found',
            message : 'There are no ratings for this product',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

/**
 * @method POST
 * @description method to add new rating for the product
 * 
 * @param req 
 * @param res 
 * @returns 
 */
export const addRating = async (req : Request, res : Response) => {
    const {error, value} = validateRating(req.params.id, req.body);
    if (error) return res.status(400).json(error.details);

    try {
        const rating = await Rating.create({
            productId : value.productId,
            userId : value.productId,
            title : value.title,
            description : value.description,
            rate : value.rate,
        })
        return res.status(201).json(rating);
    } catch (error) {
        return res.status(501).json(error);
    }
}