import { Request, Response, NextFunction } from "express";
import { verify, decode } from 'jsonwebtoken';
import User from "../models/user.model";

export const isAuthenticated = async (req : Request, res : Response, next : NextFunction) => {
    try {
        const token : any = req.headers.authorization?.toString();
        const result = verify(token, "secret-key");
        if (result) {
            return next();
        } else {
            return res.status(401).json({
                error : 'Not authorized',
                message : 'You are not authenticated'
            })
        }
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const isVendor = async (req : Request, res : Response, next : NextFunction) => {
    try {
        const token : any = req.headers.authorization;
        const parsedToken : any = decode(token)
        const data = JSON.parse(parsedToken);
        if (!data._id) {
            return res.status(400).json({
                error : 'Not authoized',
                message : "User does not have id",
            });
        }

        const user = await User.findById(data._id);
        if (!user || user.role !== 'v') {
            return res.status(401).json({
                error : "Not authorized",
                message : "You don't have permission to get this router",
            })
        }
        return next();
    } catch (error) {
        return res.status(501).json(error);
    }
}
