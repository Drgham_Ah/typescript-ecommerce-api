import dotenv from 'dotenv';
import { sign } from 'jsonwebtoken';
import mongoose from 'mongoose';

dotenv.config();

interface Payload {
    _id : String,
    name : string,
    email : string,
    phone : string,
    image? : string,
    role : string,
}

export const generateToken = (payloads : Payload) => {
    const token = sign(payloads, 'secret-key');
    return token;
}