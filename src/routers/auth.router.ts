import express from 'express';
import { userLogin, userRegister } from '../controllers/auth.controller';

const AuthRouter = express.Router();

AuthRouter.post('/customer/register', userRegister);
AuthRouter.post('/customer/login', userLogin);

export default AuthRouter;