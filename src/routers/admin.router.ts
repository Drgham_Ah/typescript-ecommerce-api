import express from 'express';
import { changePassword, createAdminCode, index, login, register, show } from '../controllers/admin.controller';

const AdminRouter = express.Router();

AdminRouter.post('/generate-admin-code', createAdminCode);
AdminRouter.post('/register', register);
AdminRouter.post('/login', login);
AdminRouter.get('/', index);
AdminRouter.get('/:id', show);
AdminRouter.post('/changepassword', changePassword);

export default AdminRouter;