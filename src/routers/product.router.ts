import express from 'express';
import { create, index, show, update, destroy } from '../controllers/product.controller';

const ProductRouter = express.Router();

ProductRouter.get('/', index);
ProductRouter.post('/', create);
ProductRouter.get('/:id', show);
ProductRouter.put('/:id', update);
ProductRouter.delete('/:id', destroy);

export default ProductRouter;