import express from 'express';
import { index, create, show, update, destroy } from '../controllers/country.controller';

const CountryRouter = express.Router();

CountryRouter.get('/', index);
CountryRouter.post('/', create);
CountryRouter.get('/:id', show);
CountryRouter.put('/:id', update);
CountryRouter.delete('/:id', destroy);

export default CountryRouter;