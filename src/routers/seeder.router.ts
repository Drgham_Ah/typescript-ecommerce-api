import express from 'express';
import { countrySeeder, seeder, stateSeeder } from '../database/seeder';

const SeederRouter = express.Router();

SeederRouter.post('/', seeder);
SeederRouter.post('/countries', countrySeeder);
SeederRouter.post('/states', stateSeeder);

export default SeederRouter;