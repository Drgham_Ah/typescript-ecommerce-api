import express from "express";
import { index, create, show, update, destroy } from "../controllers/category.controller";
import { isAuthenticated } from "../middlewares/auth.middleware";

const CategoryRouter = express.Router();

CategoryRouter.get('/', isAuthenticated, index);
CategoryRouter.post('/', create);
CategoryRouter.get('/:id', show);
CategoryRouter.put('/:id', update);
CategoryRouter.delete('/:id', destroy);

export default CategoryRouter;