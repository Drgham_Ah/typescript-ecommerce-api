import express from 'express';
import { destroyCountries, destroyStates } from '../database/destroy';

const DestroyRouter = express.Router();

DestroyRouter.post('/countries', destroyCountries);
DestroyRouter.post('/states', destroyStates);

export default DestroyRouter;