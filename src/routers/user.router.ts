import express from 'express';
import { index, create, show, update, destroy } from '../controllers/user.controller';

const UserRouter = express.Router();

UserRouter.get('/', index);
UserRouter.post('/', create);
UserRouter.get('/:id', show);
UserRouter.put('/:id', update);
UserRouter.delete('/:id', destroy);

export default UserRouter;