import express from "express";
import { index, create, show, update, destroy } from "../controllers/state.controller";

const StateRouter = express.Router();

StateRouter.get('/', index);
StateRouter.post('/', create);
StateRouter.get('/:id', show);
StateRouter.put('/:id', update);
StateRouter.delete('/:id', destroy);

export default StateRouter;