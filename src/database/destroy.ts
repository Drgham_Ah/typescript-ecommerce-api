import Country from "../models/country.model"
import {Request, Response} from 'express';
import State from "../models/state.model";

export const destroyCountries = async (req : Request, res : Response) => {
    try {
        const countries = await Country.deleteMany();
        return res.status(200).json({
            success : "Countries destoried successfully",
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const destroyStates = async (req : Request, res : Response) => {
    try {
        const states = await State.deleteMany();
        return res.status(200).json({
            success : 'States destroy successfully',
        })
    } catch (error) {
        return res.status(501).json(error);
    }
}