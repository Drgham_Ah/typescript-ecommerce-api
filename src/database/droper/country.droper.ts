import mongoose from "mongoose";
import Country from "../../models/country.model";

const countryDropper = async () => {

    try {
        const result = await Country.deleteMany();
        if (result) {
            console.log(result);
        } else {
            console.log("Something went wrong");
        }
    } catch (error) {
        console.log(error);
    }
}

mongoose.connect(`${process.env.MONGODB_URL}`)

countryDropper();