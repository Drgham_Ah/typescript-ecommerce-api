import Country from "../../models/country.model";
import countries from '../../data/countries.json';

const insterCountries = async () => {
    try {
        const result = await Country.insertMany(countries);
        if (result) {
            console.log(result);
        } else {
            console.log("Something went wrong")
        }
    } catch (error) {
        console.log(error);
    }
}

insterCountries();
