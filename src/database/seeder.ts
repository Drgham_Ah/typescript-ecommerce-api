import { Request, Response } from "express";
import Country from "../models/country.model";
import countries from '../data/countries.json';
import State from "../models/state.model";
import states from '../data/states.json';

export const seeder = async (req : Request, res : Response) => {
    try {
        const result = await Country.insertMany(countries);
        if (result) {
            const resultTwo = await State.insertMany(states);
            if (resultTwo) {
                return res.status(200).json({
                    success: 'Seeded Successfully',
                })
            }
        }
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const countrySeeder = async (req : Request, res : Response) => {
    try {
        const result = await Country.insertMany(countries);
        if (result) {
            return res.status(200).json({
                success: 'Seeded Successfully',
            })
        }
    } catch (error) {
        return res.status(501).json(error);
    }
}

export const stateSeeder = async (req : Request, res : Response) => {
    try {
        await State.deleteMany();
        await State.insertMany(states);
        return res.status(200).json({
            success : 'Seeded',
            message : 'States seeded successfully'
        })
        
    } catch (error) {
        return res.status(501).json(error);
    }
}